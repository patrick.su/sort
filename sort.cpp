/*
 * sort.cpp
 *
 *  Created on: 2013-1-27
 *      Author: suzhengkai
 */

#include "sort.h"

//static const int sort_data_size = 10;


TestSort::TestSort()
{
	int i;

	srand(unsigned(time(NULL)));
	for (i = 0; i < sort_data_size; ++i)
	{
		data[i] = rand() % 1000;
	}
}

void TestSort::data_print(int print_flag, const char *s)
{
	int i;

	if (print_flag)
	{
		cout << s << endl;

		for (i = 0; i < sort_data_size; ++i)
		{
			cout << "data[" << i << "]\t=\t" << data[i] << endl;
		}
	}
	else
	{
		if (s != NULL)
		{
			cout << s << endl;
		}
		for (i = 0; i < sort_data_size; ++i)
		{
			cout << data[i] << "\t\t";
		}

		cout << endl;
	}
}

void TestSort::print_data(int print_flag, const char *s)
{
	int i;

	if (print_flag)
	{
		cout << s << endl;

		for (i = 0; i < sort_data_size; ++i)
		{
			cout << "data[" << i << "]\t=\t" << data[i] << endl;
		}
	}
	else
	{
		if (s != NULL)
		{
			cout << s << endl;
		}
		for (i = 0; i < sort_data_size; ++i)
		{
			cout << data[i] << "\t\t";
		}

		cout << endl;
	}
}

void TestSort::swap_data(int i, int j)
{
	int tmp;

	tmp = data[i];
	data[i] = data[j];
	data[j] = tmp;
}

void TestSort::sort_bubble()
{
	int i,j;
	int flag = true;

	for (i = 0; i < sort_data_size - 1 && flag; ++i)
	{
		flag = false;
		for (j = 0; j < sort_data_size - i -1; ++j)
		{
			if (data[j] > data[j + 1])
			{
				swap_data(j, j+1);
				flag = true;
			}
		}
	}

	data_print(ROW_PRINT, "After Bubble Sort:");
}

void TestSort::sort_cocktail()
{
	int i;
	int low,high,idx;

	idx = 0;
	low = 0;
	high = sort_data_size - 1;

	while(low < high)
	{
		for (i = low; i <= high - 1; ++i)
		{
			if (data[i] > data[i + 1])
			{
				swap_data(i, i+1);
				idx = i;
			}
		}
		high = idx;

		for (i = high; i > low; --i)
		{
			if (data[i] < data[i-1])
			{
				swap_data(i, i-1);
				idx = i;
			}
		}
		low = idx;
	}

	data_print(ROW_PRINT, "After Cocktail2 Sort:");
}

int TestSort::sort_quick_part(int low, int high)
{
	int mid_val = data[low];

	while (low < high)
	{
		while (low < high && data[high] >= mid_val)
		{
			high--;
		}
		data[low] = data[high];
//		swap_data(low, high);
//		data_print(ROW_PRINT);

		while(low < high && data[low] <= mid_val)
		{
			low++;
		}
//		swap_data(low, high);
		data[high] = data[low];
//		data_print(ROW_PRINT);
	}

	data[low] = mid_val;
	return low;
}

void TestSort::sort_quick(int low, int high)
{
	int mid;

	if (low < high)
	{
		mid = sort_quick_part(low, high);
		sort_quick(low, mid - 1);
		sort_quick(mid +1, high);
	}
}

void TestSort::sort_quick2(int low, int high)
{
	int mid;
	int l,h;
	stack<int> p_int;

	if (low < high)
	{
		p_int.push(low);
		p_int.push(high);

		while(!p_int.empty())
		{
			h = p_int.top();
			p_int.pop();
			l = p_int.top();
			p_int.pop();

			mid = sort_quick_part(l, h);

			if (l < mid - 1)
			{
				p_int.push(l);
				p_int.push(mid - 1);
			}

			if (mid+1 < h)
			{
				p_int.push(mid + 1);
				p_int.push(h);
			}
		}
	}

	data_print(ROW_PRINT, "After Quick(N) Sort:");
}

void TestSort::sort_insert()
{
	int i,j;
	int val;

	for (i = 1; i < sort_data_size; ++i)
	{
		if (data[i] < data[i - 1])
		{
			val = data[i];

			for (j = i - 1; j >= 0 && data[j] > val; --j)
			{
				data[j + 1] = data[j];
			}
			data[j + 1] = val;
		}
	}

	data_print(ROW_PRINT, "After Insert Sort:");
}

void TestSort::sort_select()
{
	int i,j,min;

	for (i = 0; i < sort_data_size - 1; ++i)
	{
		min = i;
		for (j = i + 1; j < sort_data_size; ++j)
		{
			if (data[min] > data[j])
			{
				min = j;
			}
		}

		if (min != i)
		{
			swap_data(i, min);
		}
	}

	data_print(ROW_PRINT, "After Selec Sort:");
}

void TestSort::sort_shell()
{
	int i,j;
	int val;
	int increment = sort_data_size;

	do
	{
		increment = increment / 3 + 1;

		for (i = increment; i < sort_data_size; ++i)
		{
			if (data[i] < data[i - increment])
			{
				val = data[i];
				for (j = i - increment; j >= 0 && data[j] > val; j -= increment)
				{
					data[j + increment] = data[j];
				}
				data[j + increment] = val;
			}
		}
	} while (increment > 1);

	data_print(ROW_PRINT, "After Shell Sort:");
}

void TestSort::sort_heap()
{
	int i;

	for (i = (sort_data_size - 1)/2; i >= 0 ; --i)
	{
		sort_heap_adjust(i, sort_data_size);
	}

	for (i = sort_data_size - 1; i > 0; --i)
	{
		swap_data(0, i);
		sort_heap_adjust(0, i);
	}

	data_print(ROW_PRINT, "After Heap Sort:");
}

void TestSort::sort_heap_adjust(int start, int len)
{
	int i;
	int val;

	val = data[start];

	for (i = start*2 + 1; i < len; i = i*2 +1)
	{
		if (data[i] < data[i + 1] && (i + 1) < len)
		{
			i++;
		}

		if (val >= data[i])
		{
			break;
		}

		data[start] = data[i];
		start = i;
	}
	data[start] = val;
}


void TestSort::sort_merge_array(int s, int m, int e)
{
	int tmp[sort_data_size] = {};
	int i,j,k;

	i = 0;
	j = s;
	k = m+1;

	while(j <= m && k <= e )
	{
		if (data[k] >= data[j])
		{
			tmp[i++] = data[j++];
		}
		else
		{
			tmp[i++] = data[k++];
		}
	}

	while(j <= m)
	{
		tmp[i++] = data[j++];
	}

	while(k <= e)
	{
		tmp[i++] = data[k++];
	}

	for (j = 0; j < i; ++j)
	{
		data[s+j] = tmp[j];
	}
}

void TestSort::sort_merge(int s, int e)
{
	int mid = (s + e) / 2;

	if (s < e)
	{
		sort_merge(s, mid);
		sort_merge(mid+1, e);
		sort_merge_array(s, mid, e);
	}
}

void TestSort::sort_bubble_20130531()
{

}

void TestSort::sort_cocktail_20130531()
{

}

void TestSort::sort_select_20130531()
{

}

void TestSort::sort_insert_20130531()
{

}

void TestSort::sort_shell_20130531()
{

}

void TestSort::sort_quick_20130531(int low, int high)
{

}

int TestSort::sort_quick_part_20130531(int low, int high)
{

}

void TestSort::sort_heap_20130531()
{

}

void TestSort::sort_heap_adjust_20130531(int s, int len)
{

}


void TestSort::sort_merge_20130531(int s, int e)
{

}

void TestSort::sort_merge_array_20130531(int s, int m, int e)
{

}
