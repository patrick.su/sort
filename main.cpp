/*
 * main.cpp
 *
 *  Created on: 2013-1-27
 *      Author: suzhengkai
 */
#include "sort.h"

#include <stdio.h>
int main()
{
	TestSort InsertObj;
	TestSort BubbleObj;
	TestSort QuickObj;
	TestSort QuickNObj;
	TestSort SelectObj;
	TestSort ShellObj;

	TestSort HeapObj;
	TestSort CocktailObj;
	TestSort MergeObj;

	BubbleObj.data_print(ROW_PRINT);
	CocktailObj.sort_cocktail_20130531();
	CocktailObj.data_print(ROW_PRINT, "After Cocktail Sort:");

	BubbleObj.sort_bubble_20130531();
	BubbleObj.data_print(ROW_PRINT, "After Bubble Sort:");

	SelectObj.sort_select_20130531();
	SelectObj.data_print(ROW_PRINT, "After Select Sort:");

	InsertObj.sort_insert_20130531();
	InsertObj.data_print(ROW_PRINT, "After Insert Sort:");

	ShellObj.sort_shell_20130531();
	ShellObj.data_print(ROW_PRINT, "After Shell Sort:");

	HeapObj.sort_heap_20130531();
	HeapObj.data_print(ROW_PRINT, "After Heap Sort:");

	QuickObj.sort_quick_20130531(0, sort_data_size-1);
	QuickObj.data_print(ROW_PRINT, "After Quick Sort:");

	MergeObj.sort_merge_20130531(0, sort_data_size - 1);
	MergeObj.print_data(ROW_PRINT, "After Merge Sort:");

	QuickNObj.sort_quick2(0, sort_data_size-1);




	return 0;
}


