/*
 * sort.h
 *
 *  Created on: 2013-1-27
 *      Author: suzhengkai
 */

#ifndef SORT_H_
#define SORT_H_


#include <iostream>
#include <time.h>
#include <cstdlib>
#include <stack>

using namespace std;

static const int sort_data_size = 10;

#define ROW_PRINT	0
#define COL_PRINT	1

#define SORT_QUICK_MIN_CNT	7

class TestSort
{
public:

	TestSort();
//	~TestSort();

	void data_print(int print_flag, const char *s = "Data Initial:");
	void print_data(int print_flag, const char *s = "Data Initial:");

	void sort_bubble();
	void sort_cocktail();

	int sort_quick_part(int low, int high);
	void sort_quick(int low, int high);
	void sort_quick2(int low, int high);

	void sort_insert();
	void sort_select();
	void sort_shell();

	void sort_heap_adjust(int start, int len);
	void sort_heap();

	void sort_merge_array(int s, int m, int e);
	void sort_merge(int s, int e);


	void sort_bubble_20130531();
	void sort_cocktail_20130531();
	void sort_insert_20130531();
	void sort_select_20130531();
	void sort_shell_20130531();

	int sort_quick_part_20130531(int low, int high);
	void sort_quick_20130531(int low, int high);

	void sort_heap_adjust_20130531(int start, int len);
	void sort_heap_20130531();

	void sort_merge_array_20130531(int s, int m, int e);
	void sort_merge_20130531(int s, int e);

private:
	int data[sort_data_size];

	void swap_data(int i, int j);
};

namespace TypeSort
{

}

#endif /* SORT_H_ */
